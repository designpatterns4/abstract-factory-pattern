package com.designpaterns;

public class ThinCrustDough implements Dough {
    public String toString() {
        return "Thin Crust Dough";
    }
}
