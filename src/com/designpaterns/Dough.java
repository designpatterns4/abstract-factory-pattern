package com.designpaterns;

public interface Dough {
    public String toString();
}
