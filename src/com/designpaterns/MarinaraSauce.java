package com.designpaterns;

public class MarinaraSauce implements Sauce {
    public String toString() {
        return "Marinara Sauce";
    }
}
