package com.designpaterns;

public interface Pepperoni {
    public String toString();
}
