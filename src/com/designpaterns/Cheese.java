package com.designpaterns;

public interface Cheese {
    public String toString();
}
