package com.designpaterns;

public interface Sauce {
    public String toString();
}
